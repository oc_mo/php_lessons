<?php


namespace app\controllers;


use yii\web\Controller;
use app\models\Good;
use app\models\Cart;
use Yii;
use app\models\Order;
use app\models\OrderGood;
use yii\helpers\Url;

class CartController extends Controller {

    public function actionOrder() {
        $session = Yii::$app->session;
        $session -> open();
        if (!$session['cart.totalSum']){
            return Yii::$app->response->redirect(Url::to('/'));
        }
        $order = new Order();
        if ($order -> load(Yii::$app->request->post())){
            $order->date = date('Y-m-d H:i:s');
            $order->sum = $session['cart.totalSum'];
            if ($order->save()) {
                $currentID = $order->id;
                $this->saveOrderInfo($session['cart'], $currentID);
                Yii::$app->mailer->compose('order-mail',['session'=>$session, 'order'=>$order])
                    ->setFrom(['tforphp@gmail.com'=>'test.test'])
                    ->setTo($order->email)
                    ->setSubject('Ваш заказ приянт')
                    ->send();
                $session ->remove('cart');
                $session ->remove('cart.totalQuantity');
                $session ->remove('cart.totalSum');
                return $this->render('success', compact('session', 'currentID'));

            }
        }
        $this->layout = 'empty-layout';
        return $this->render('order', compact('session', 'order'));

    }

    protected function saveOrderInfo($goods, $orderID) {
        foreach ($goods as $id=>$good) {
            $orderInfo = new OrderGood();
            $orderInfo->order_id = $orderID;
            $orderInfo->product_id = $id;
            $orderInfo->name = $good['name'];
            $orderInfo->price = $good['price'];
            $orderInfo->quantity = $good['goodQuantity'];
            $orderInfo->sum = $good['price']*$good['goodQuantity'];
            $orderInfo->save();
        }
    }

    public function actionClear() {
        $session = Yii::$app->session;
        $session -> open();
        $session ->remove('cart');
        $session ->remove('cart.totalQuantity');
        $session ->remove('cart.totalSum');
        return $this->renderPartial('cart', compact('session'));
    }

    public function actionAdd($name) {
        $good = new Good();
        $good = $good->getOneGood($name);
        $session = Yii::$app->session;
        $session -> open();
       // $session ->remove('cart');
        $cart = new Cart();
        $cart->addToCart($good);
        return $this->renderPartial('cart', compact('good', 'session'));
    }

    public function  actionOpen() {
        $session = Yii::$app->session;
        $session -> open();
        return $this->renderPartial('cart', compact('session'));
    }

    public  function actionDelete($id) {
        $session = Yii::$app->session;
        $session -> open();
        $cart = new Cart();
        $cart ->recalcCart($id);
        return $this->renderPartial('cart', compact('session'));
    }
}
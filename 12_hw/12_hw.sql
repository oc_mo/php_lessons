create table orders
(
    orders_id    int auto_increment
        primary key,
    orders_email varchar(40) not null,
    orders_date  datetime    not null,
    constraint FK_email_user_order
        foreign key (orders_email) references users (email)
            on update cascade
);

INSERT INTO main.orders (orders_id, orders_email, orders_date) VALUES (1, 'qq@gmail.nz', '2019-12-04 03:30:23');
INSERT INTO main.orders (orders_id, orders_email, orders_date) VALUES (2, 'olleg@web.com', '2019-12-04 03:32:08');
INSERT INTO main.orders (orders_id, orders_email, orders_date) VALUES (3, 'fred@mail.KaJ', '2019-12-04 03:32:29');
create table users
(
    id    int auto_increment
        primary key,
    name  varchar(60) not null,
    email varchar(40) not null,
    constraint email_UNIQUE
        unique (email)
);

INSERT INTO main.users (id, name, email) VALUES (1, 'Фред', 'fred@mail.KaJ');
INSERT INTO main.users (id, name, email) VALUES (2, 'Олег', 'olleg@web.com');
INSERT INTO main.users (id, name, email) VALUES (3, 'Фред', 'qq@gmail.nz');
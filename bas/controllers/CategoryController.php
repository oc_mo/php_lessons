<?php


namespace app\controllers;

use app\models\Good;
use yii\web\Controller;
use Yii;

class CategoryController extends Controller
{
    public function actionIndex(){
        $goods = new Good();
        $goods = $goods->getAllGoods();
        return $this->render('index', compact('goods'));
    }

    public function actionView($id){
        $catGoods = new Good();
        $catGoods = $catGoods->getGoodsCategories($id);
        return $this->render('view', compact('catGoods'));
    }

    public function actionSearch() {
        $search = strip_tags(Yii::$app->request->get('search'));
        $goods = new Good();
        $goods = $goods->getSearchGoodsResults($search);
        return $this->render('search', compact('goods','search'));
    }
}
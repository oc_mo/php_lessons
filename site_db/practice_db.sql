create table about_career
(
    id    int auto_increment
        primary key,
    lorem longtext not null
)
    comment 'Описание карьеры';

INSERT INTO practice_db.about_career (id, lorem) VALUES (1, 'lorem lorem lorem');
create table aboute
(
    id    int auto_increment
        primary key,
    name  varchar(30) not null,
    post  varchar(30) not null,
    email varchar(30) not null,
    phone varchar(30) not null,
    site  varchar(50) not null
)
    comment 'Описание';

INSERT INTO practice_db.aboute (id, name, post, email, phone, site) VALUES (1, 'Артём Ларин', 'Ламер', 'normal@mail.true', '+19651453025', 'google.com');
create table career
(
    id        int auto_increment
        primary key,
    post      varchar(50) not null,
    place     varchar(50) not null,
    duty      text        not null,
    yearStart int         not null,
    yearEnd   varchar(30) null
)
    comment 'Места работы';

INSERT INTO practice_db.career (id, post, place, duty, yearStart, yearEnd) VALUES (1, 'post1', 'City1', 'Lorem Lorem1', 2016, 'настоящие время');
INSERT INTO practice_db.career (id, post, place, duty, yearStart, yearEnd) VALUES (2, 'post2', 'City2', 'Lorem Lorem12', 2011, '2015');
INSERT INTO practice_db.career (id, post, place, duty, yearStart, yearEnd) VALUES (3, 'post3', 'City3', 'Lorem Lorem123', 2006, '2010');
create table comments
(
    id       int auto_increment
        primary key,
    comments varchar(255) not null
)
    comment 'Комментарии';


create table education
(
    id         int auto_increment
        primary key,
    faculty    varchar(100) not null,
    university varchar(100) not null,
    yearStart  int          not null,
    yearEnd    varchar(50)  not null
)
    comment 'Сведения об образовании';

INSERT INTO practice_db.education (id, faculty, university, yearStart, yearEnd) VALUES (1, 'Программирование в компьютерных системах', 'ГБОПУ СРМК', 2016, 'По настоящие время');
create table interests
(
    id            int auto_increment
        primary key,
    interestsName varchar(50) not null
)
    comment 'Интересы';

INSERT INTO practice_db.interests (id, interestsName) VALUES (1, 'interests 1');
INSERT INTO practice_db.interests (id, interestsName) VALUES (2, 'interests 2');
create table languages
(
    id   int auto_increment
        primary key,
    titl varchar(40) not null
)
    comment 'Изученные языки';

INSERT INTO practice_db.languages (id, titl) VALUES (1, 'Russian (Native)');
INSERT INTO practice_db.languages (id, titl) VALUES (2, 'English (Foreign)');
create table project
(
    id          int auto_increment
        primary key,
    site        varchar(60) not null,
    projectName varchar(60) not null,
    description text        not null
)
    comment 'Разработанные проекты';

INSERT INTO practice_db.project (id, site, projectName, description) VALUES (1, 'http://google.com', 'Project1', 'Lorem1');
INSERT INTO practice_db.project (id, site, projectName, description) VALUES (2, 'http://vk.ru', 'Project12', 'Lorem12');
INSERT INTO practice_db.project (id, site, projectName, description) VALUES (3, 'http://yandex.ru', 'Project123', 'Lorem123');
create table skills
(
    id    int auto_increment
        primary key,
    skill varchar(30) not null,
    level varchar(4)  not null
)
    comment 'Уровень умений';

INSERT INTO practice_db.skills (id, skill, level) VALUES (1, 'C#', '55%');
INSERT INTO practice_db.skills (id, skill, level) VALUES (2, 'PHP', '99%');
-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Дек 02 2019 г., 16:12
-- Версия сервера: 8.0.15
-- Версия PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `library`
--

-- --------------------------------------------------------

--
-- Структура таблицы `books`
--

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `author` varchar(50) NOT NULL,
  `year_publishing` year(4) NOT NULL,
  `rating` tinyint(1) NOT NULL,
  `readed` tinyint(1) NOT NULL DEFAULT '0',
  `color` varchar(15) NOT NULL DEFAULT 'красный',
  `pages` smallint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `books`
--

INSERT INTO `books` (`id`, `title`, `author`, `year_publishing`, `rating`, `readed`, `color`, `pages`) VALUES
(1, 'Будосёсинсю', 'Дайдодзи Юдзан ', 2017, 5, 1, 'чёрный', 98),
(2, 'Хагакурэ', 'Ямамото Цунэтомо ', 2016, 5, 1, 'малохитовый', 120),
(3, 'Юкио Мисима ', 'Хагакурэ Нюмон', 2016, 5, 1, 'зелённый ', 130),
(4, 'Зов Ктулху', 'Говард Лавкрафт', 1928, 4, 1, 'белый ', 96),
(5, 'Хребты безумия', 'Говард Лавкрафт', 1936, 3, 1, 'Голубой', 150),
(6, 'Зов Ктулху', 'Дагон ', 1919, 5, 1, 'Синий', 84),
(7, 'Ужас Данвича', 'Говард Лавкрафт', 1929, 5, 1, 'серый', 140);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

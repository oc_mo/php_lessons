<?php
session_start();

include 'data.php';

$comments = $connection->query('SELECT * FROM practice_db.forum_comments WHERE moderation = "ok" ORDER by date DESC');

if ($_POST['text']) {
    $userName = htmlspecialchars($_POST['userName']);
    $text = htmlspecialchars($_POST['text']);
    $time = date("Y-m-d H:i:s");
    $newComment = $connection->prepare("INSERT INTO `practice_db`.`forum_comments` (user_name, comment) VALUES (:user_name, :text)");
    $arr = ['user_name'=>$userName, 'text'=>$text];
    $newComment->execute($arr);
    header('Location: forum.php');
}
?>
<!doctype html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>
    </head>
    <body>
    <style>
        body {
            margin: 50px;
            font-family: Arial, sans-serif;
        }

        input, textarea, button {
            margin: 15px;
            display: block;
            font-size: 30px;
        }
    </style>
    <h1>Форум</h1>
    <form action="" method="POST">
        <input type="text" name="userName" required placeholder="Ваше имя">
        <textarea type="text" name="text" required placeholder="Ваше сообещение" cols="30" rows="5"></textarea>
        <button>Отправть</button>
    </form>
    <hr>
    <h2>Сообщения пользователей</h2>
    <h3>Все сообщения модерируюься</h3>
    </body><?php
if ($comments) {
    foreach ($comments as $comment){
        echo $comment['data']. ' '. $comment['user_name'].' '.' написал '. $comment['comment']. '<hr><br>';

    }
}
?>
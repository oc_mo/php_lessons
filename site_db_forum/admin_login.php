<?php
session_start();
include "data.php";
$data = $connection->query("SELECT * FROM practice_db.admin");
if ($_POST['login']) {
    foreach ($data as $info) {
        if ($_POST ['login'] == $info['login'] && $_POST['password'] == $info['password']) {
            $_SESSION['login'] = $_POST['login'];
            $_SESSION['password'] = $_POST['password'];
            $_SESSION['rang'] = $info['rang'];
            header('Location: admin_panel.php');
    }
    }
}
?>

<style>
        body {
            margin: 50px;
            font-family: Arial, sans-serif;
        }

        input, textarea, button {
            margin: 15px;
            display: block;
            font-size: 30px;
        }
</style>

<h2>Вход в админку</h2>

<form action="" method="POST">
    <input type="text" name="login" placeholder="Логин" required>
    <input type="password" name="password" placeholder="Пароль" required>
    <button>Отправть</button>
</form>

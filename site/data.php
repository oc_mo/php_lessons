<?php

return[
    'about'=>[
        'name'=>'Артём Ларин',
        'post'=>'Ламер',
        'email'=>'normal@mail.true',
        'phone'=>'+19651453025',
        'site'=>'google.com'
    ],
        'education'=>[
            [
                'faculty'=>'Программирование в компьютерных системах',
                'univercity' =>'ГБОПУ СРМК',
                'yearStart'=>2016,
                'yearEnd'=>2020
            ]
        ],
    'languages'=>[
             'Russian (Native)','English (Foreign)'
     ],
    'interests'=>[
        'interests 1', 'interests 2'
    ],
    'aboutCareer' => 'Lorem  Lorem',

    'Career'=>[
        [
            'post' => 'post1',
            'place'=>'City1',
            'duty'=>'Lorem Lorem1',
            'yearStart'=>2016,
            'yearEnd'=>'наст время',
        ],
        [
            'post' => 'post2',
            'place'=>'City2',
            'duty'=>'Lorem lorem12',
            'yearStart'=>2010,
            'yearEnd'=>2015
        ],
        [
            'post' => 'post3',
            'place'=>'City3',
            'duty'=>'Lorem lorem123',
            'yearStart'=>2005,
            'yearEnd'=>2009
        ]

    ],
    'Project'=>[
        [
            'Site' => 'http://google.com',
            'NProject' => 'Project1',
            'Dec' => 'Lorem1',
        ],
        [
            'Site' => 'http://vk.ru',
            'NProject' => 'Project2',
            'Dec' => 'Lorem2',
        ],
        [
            'Site' => 'http://yandex.ru',
            'NProject' => 'Project3',
            'Dec' => 'Lorem3',
        ]
    ],
    'Skills'=>[
        [
          'skill'=>'PHP',
          'level'=>'148%'
        ],
        [
          'skill'=>'C#',
          'level'=>'45%'
        ]

    ]

];

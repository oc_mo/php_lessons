<?php

class Product
{
    public $name;
    public $price;
    public $weight;
    public $taxFree;

    function __construct(string $name,int $price,int $weight)
    {
        $this->name=$name;
        $this->price=$price;
        $this->weight=$weight;
    }

    function output ()
    {
        echo 'Название товара: '.$this->name.'<br> Цена товара: '.$this->price.' рубелй <br> Вес товара: '.$this->weight.' кг<br><br>';
    }

    function taxFree ()
    {
        $tax=$this->price-$this->price*0.2;
        echo 'Название товара:'.$this->name.'<br> Цена товара c НДС: '.$this->price.' рубелй <br> Цена товара без НДС: '.$tax.' рублей <br><br>';
    }
}
$abouteSpaghetti = new Product('Макороны','250','1');
$aboutePotato = new Product('Картошка','300','10');
$abouteRis = new Product('Рис','300','5');
$abouteSpaghetti->output();
$abouteSpaghetti->taxFree();
$aboutePotato->output();
$aboutePotato->taxFree();
$abouteRis->output();
$abouteRis->taxFree();

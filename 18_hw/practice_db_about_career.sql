create table about_career
(
    id    int auto_increment
        primary key,
    lorem longtext not null
)
    comment 'Описание карьеры';

INSERT INTO practice_db.about_career (id, lorem) VALUES (1, 'lorem lorem lorem');
create table aboute
(
    id    int auto_increment
        primary key,
    name  varchar(30) not null,
    post  varchar(30) not null,
    email varchar(30) not null,
    phone varchar(30) not null,
    site  varchar(50) not null
)
    comment 'Описание';

INSERT INTO practice_db.aboute (id, name, post, email, phone, site) VALUES (1, 'Артём Ларин', 'Ламер', 'normal@mail.true', '+19651453025', 'google.com');
create table admin
(
    id       int auto_increment
        primary key,
    login    varchar(30) not null,
    password varchar(30) not null,
    rang     varchar(45) null
);

INSERT INTO practice_db.admin (id, login, password, rang) VALUES (1, 'admin', '123', 'admin');
INSERT INTO practice_db.admin (id, login, password, rang) VALUES (3, 'moder', '123', 'moder');
create table career
(
    id        int auto_increment
        primary key,
    post      varchar(50) not null,
    place     varchar(50) not null,
    duty      text        not null,
    yearStart int         not null,
    yearEnd   varchar(30) null
)
    comment 'Места работы';

INSERT INTO practice_db.career (id, post, place, duty, yearStart, yearEnd) VALUES (1, 'post1', 'City1', 'Lorem Lorem1', 2016, 'настоящие время');
INSERT INTO practice_db.career (id, post, place, duty, yearStart, yearEnd) VALUES (2, 'post2', 'City2', 'Lorem Lorem12', 2011, '2015');
INSERT INTO practice_db.career (id, post, place, duty, yearStart, yearEnd) VALUES (3, 'post3', 'City3', 'Lorem Lorem123', 2006, '2010');
create table comments
(
    id      int          not null
        primary key,
    comment varchar(100) not null
);


create table education
(
    id         int auto_increment
        primary key,
    faculty    varchar(100) not null,
    university varchar(100) not null,
    yearStart  int          not null,
    yearEnd    varchar(50)  not null
)
    comment 'Сведения об образовании';

INSERT INTO practice_db.education (id, faculty, university, yearStart, yearEnd) VALUES (1, 'Программирование в компьютерных системах', 'ГБОПУ СРМК', 2016, 'По настоящие время');
create table forum_comments
(
    id         int auto_increment
        primary key,
    user_name  varchar(50)                            not null,
    comment    varchar(255)                           not null,
    date       timestamp    default CURRENT_TIMESTAMP not null,
    moderation varchar(100) default 'new'             not null
)
    comment 'Коммиентраии с форума';

INSERT INTO practice_db.forum_comments (id, user_name, comment, date, moderation) VALUES (1, 'qwe', 'qwet', '2019-12-09 21:15:29', 'ok');
INSERT INTO practice_db.forum_comments (id, user_name, comment, date, moderation) VALUES (3, 'dsdsd', 'sdsdsdfe', '2019-12-09 21:23:08', 'rejected');
INSERT INTO practice_db.forum_comments (id, user_name, comment, date, moderation) VALUES (8, 'Test', 'Test', '2019-12-09 21:44:39', 'ok');
INSERT INTO practice_db.forum_comments (id, user_name, comment, date, moderation) VALUES (9, 'Artem', 'Первое сообщение', '2019-12-09 22:34:31', 'ok');
INSERT INTO practice_db.forum_comments (id, user_name, comment, date, moderation) VALUES (10, 'user_name=:userName', 'comment=:text', '2019-12-10 00:26:21', 'rejected');
INSERT INTO practice_db.forum_comments (id, user_name, comment, date, moderation) VALUES (18, '213', '123', '2019-12-10 00:55:59', 'rejected');
create table images
(
    id        int auto_increment
        primary key,
    img_name  varchar(100) not null,
    extension varchar(8)   not null
);

INSERT INTO practice_db.images (id, img_name, extension) VALUES (3, '145458292112119207[1]', 'jpg');
INSERT INTO practice_db.images (id, img_name, extension) VALUES (4, 'abb6a800ab2193fcedd9bda566b7402c[2]', 'jpg');
INSERT INTO practice_db.images (id, img_name, extension) VALUES (5, '0_PO0S0tX7ZbEucaIu', 'png');
INSERT INTO practice_db.images (id, img_name, extension) VALUES (6, '6b7ad2_av9ldyoit-o', 'jpg');
INSERT INTO practice_db.images (id, img_name, extension) VALUES (7, '075b2391a8cc156a8f0eaa9602d69c39', 'jpg');
INSERT INTO practice_db.images (id, img_name, extension) VALUES (8, 'Ax0Kfaj', 'jpg');
create table interests
(
    id            int auto_increment
        primary key,
    interestsName varchar(50) not null
)
    comment 'Интересы';

INSERT INTO practice_db.interests (id, interestsName) VALUES (1, 'interests 1');
INSERT INTO practice_db.interests (id, interestsName) VALUES (2, 'interests 2');
create table languages
(
    id   int auto_increment
        primary key,
    titl varchar(40) not null
)
    comment 'Изученные языки';

INSERT INTO practice_db.languages (id, titl) VALUES (1, 'Russian (Native)');
INSERT INTO practice_db.languages (id, titl) VALUES (2, 'English (Foreign)');
create table project
(
    id          int auto_increment
        primary key,
    site        varchar(60) not null,
    projectName varchar(60) not null,
    description text        not null
)
    comment 'Разработанные проекты';

INSERT INTO practice_db.project (id, site, projectName, description) VALUES (1, 'http://google.com', 'Project1', 'Lorem1');
INSERT INTO practice_db.project (id, site, projectName, description) VALUES (2, 'http://vk.ru', 'Project12', 'Lorem12');
INSERT INTO practice_db.project (id, site, projectName, description) VALUES (3, 'http://yandex.ru', 'Project123', 'Lorem123');
create table skills
(
    id    int auto_increment
        primary key,
    skill varchar(30) not null,
    level varchar(4)  not null
)
    comment 'Уровень умений';

INSERT INTO practice_db.skills (id, skill, level) VALUES (1, 'C#', '55%');
INSERT INTO practice_db.skills (id, skill, level) VALUES (2, 'PHP', '99%');
create table users
(
    id       int auto_increment
        primary key,
    login    varchar(30) not null,
    password varchar(30) not null,
    email    varchar(60) not null,
    constraint users_login_uindex
        unique (login)
);

INSERT INTO practice_db.users (id, login, password, email) VALUES (1, '23', '23', '23');
INSERT INTO practice_db.users (id, login, password, email) VALUES (2, 'Artem', '123', '123');
INSERT INTO practice_db.users (id, login, password, email) VALUES (3, 'Oleg', '123', 'Oleg@mail.com');
INSERT INTO practice_db.users (id, login, password, email) VALUES (4, 'Dima', 'Dima', 'Dima@mail.ru');
INSERT INTO practice_db.users (id, login, password, email) VALUES (5, 'Test', 'test', 'test');
INSERT INTO practice_db.users (id, login, password, email) VALUES (43, 'Google', '123', 'google');
<?php
session_start();
include "data.php";
if (!$_SESSION['login']|| !$_SESSION['password']) {
    header('Location: admin_login.php');
    die();
}

if ($_POST['unlogin']) {
    session_destroy();
    header('Location: admin_login.php');
}
$comments = $connection->query('SELECT * FROM practice_db.forum_comments WHERE moderation = "new" ORDER by date DESC');

if (count ($_POST)>0) {
    header('Location: admin_panel.php');
}
?>

<style>
    body {
        margin: 50px;
        font-family: Arial, sans-serif;
    }

    input, textarea, button {
        margin: 15px;
        display: block;
        font-size: 30px;
    }
    select, option,label {
        font-size: 24px;
    }
</style>


<?php
if ($_SESSION['rang']=='admin'):
?>

<h1>Панель администратора</h1>

<form method="POST">
    <?php foreach ($comments as $comment): ?>
    <select name="<?=$comment['id'];?>" id="<?=$comment['id'];?>">
        <option value="ok">Принять</option>
        <option value="rejected">Отклонить</option>
    </select>
    <label for="<?=$comment['id'];?>"><?=$comment['user_name'].' оставил комментарий "'.$comment['comment']. '"<br>';?></label>
    <?php endforeach;?>
    <button>Принять</button>
</form>

<hr>
<form action="" method="POST">
    <input type="submit" name="unlogin" value="Выйти из аккаунта">
</form>

<?php elseif ($_SESSION['rang']=='moder'):?>

<h1>Панель модератора</h1>

    <form method="POST">
        <?php foreach ($comments as $comment): ?>
            <select name="<?=$comment['id'];?>" id="<?=$comment['id'];?>">
                <option value="rejected">Отклонить</option>
            </select>
            <label for="<?=$comment['id'];?>"><?=$comment['user_name'].' оставил комментарий "'.$comment['comment']. '"<br>';?></label>
        <?php endforeach;?>
        <button>Принять</button>
    </form>

<?php endif; ?>

    <hr>
    <form action="" method="POST">
        <input type="submit" name="unlogin" value="Выйти из аккаунта">
    </form>

<?php
foreach ($_POST as $num=>$checked) {
    if ($checked == "ok") {
        $connection->query("UPDATE `practice_db`.`forum_comments` SET moderation='ok' WHERE id='$num'");
    } else {
        $connection->query("UPDATE `practice_db`.`forum_comments` SET moderation='rejected' WHERE id='$num'");
    }

}

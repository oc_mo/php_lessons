<?php

//Задание 9(
interface iall
{
    function allIn();
}
//)

abstract class Product implements iall
{
    public $name;
    public $price;
    public $weight;
    public $image;

    public static $companyName= 'Вкусные сладости';
    const YEAR = 2019;
    //Задание 9(

    static function companyInfo()
    {
        echo "Название копании: ".self::$companyName."<br> Год основания ".self::YEAR.'<br>';
    }

    function  allIn()
    {
        $this->output();
        $this->taxFree();
        $this->showImage();
    }

    //)
    function __construct(string $name,int $price,int $weight, string $image)
    {
        $this->name=$name;
        $this->price=$price;
        $this->weight=$weight;
        $this->image=$image;
    }

    function output ()
    {
        echo 'Название товара: '.$this->name.' <br> Вес товара: '.$this->weight;
    }

    function taxFree ()
    {
        $tax=$this->price-$this->price*0.2;
        echo '<br> Цена товара c НДС: '.$this->price.' рубелй <br> Цена товара без НДС: '.$tax.' рублей <br><br>';
    }

    abstract public function showImage();


}
class Chocolate extends Product
{
    protected $calories;
    public function __construct(string $name, int $price, int $weight, int $calories, string $image)
    {
        $this->calories = $calories;
        parent::__construct($name, $price, $weight, $image);
    }

    public function output ()
    {
        echo 'Название товара: '.$this->name.'<br> Вес товара: '.$this->weight.' грамм<br> Калорийность: '.$this->calories;
    }

    public function showImage()
    {
        echo "<div>
        <img src='{$this->image}' alt='Шоколадка' width='200' height='200'>
        </div>";
    }

    function __set($name, $value)
    {
        echo "Вы пытлись съесть $value кг $name";
    }
}

class Candy extends  Product
{
    public function showImage()
    {
        echo "<div>
        <img src='{$this->image}' alt='Конфеты' width='100' height='100'>
        </div>";
    }

    // Задание 9(
    function __get($name)
    {
        echo ("<br>Вы пытались съесть $name<br>");
    }
    //)
}
//Задание 9(
echo 'Вывод через обращение к классу <br>';
Product::companyInfo();
echo '<br>Вывод через обращение к дочернему объекту <br>';
$choco1 = new Chocolate('Вкусная шоколадка','140','250','300','choco.jpg');
$choco1->companyInfo();
echo '<br>';
$choco1->allIn();
//)

$candy1 = new Candy('Вкусные конфеты','500','1000','candy.jpg');
$candy1->output();
$candy1->taxFree();
$candy1->showImage();

echo '<br>Задание 9';
//Здание 9 доп(
$candy1->mentos;
$choco1->iceCream=40;
//)

<?php

$connection = new PDO ('mysql:host=localhost; dbname=practice_db; charset=utf8','root','');

if (isset($_POST['submit'])) {
    $fileName = $_FILES['file']['name'];
    $fileTmpName = $_FILES['file']['tmp_name'];
    $fileType = $_FILES['file']['type'];
    $fileError = $_FILES['file']['error'];
    $fileSize = $_FILES['file']['size'];

    $fileExtension = strtolower(end(explode('.', $fileName)));
    $fileName = implode('.', explode('.', $fileName,-1));
    $allowedExtension = ['jpg', 'jpeg', 'png'];

    if (in_array($fileExtension, $allowedExtension)){
        if ($fileSize < 300000) {
            if ($fileError==0) {
                $connection->query("INSERT INTO `practice_db`.`images` (img_name, extension) VALUES ('$fileName', '$fileExtension')");
                $lastID = $connection->query("SELECT MAX(id) FROM `practice_db`.`images`");
                $lastID = $lastID->fetchAll();
                $lastID = $lastID[0][0];
                $fileNameNew = $lastID .'_'. $fileName .'.'. $fileExtension;
                $fileDestination = 'uploads/'. $fileNameNew;
                move_uploaded_file($fileTmpName, $fileDestination);
                echo 'Файл загружен';
            } else {
                echo 'Что-то пошло не так';
            }
        } else {
            echo 'Сликом большой тип файла';
        }
    } else {
        echo 'Неверный тип файла';
    }
}

$data = $connection->query("SELECT * FROM practice_db.images");
echo "<div style='display: flex; align-items: flex-end; flex-wrap: wrap'>";
foreach ($data as $img) {
    $delete = "delete".$img['id'];
    $image = "uploads/".$img['id'].'_'.$img['img_name'].'.'.$img['extension'];

    if (isset($_POST[$delete])) {
        $imageID=$img['id'];
        $connection->query("DELETE FROM practice_db.images WHERE id = '$imageID'");
        if (file_exists($image)) {
            unlink($image);
        }
    }

    if (file_exists($image)) {
        echo "<div>";
        echo "<img width='150' height='150' src='$image'>";
        echo "<form method='POST'><button name='delete".$img['id']."' style='display: block; margin: auto'>Удалить</button></form></div>";
    }
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<form method="POST" enctype="multipart/form-data">
    <input type="file" name="file" required>
    <button name="submit">Отправить</button>
</form>
</body>
</html>

create table email
(
    id         int auto_increment
        primary key,
    user_name  varchar(50)                          not null,
    email      varchar(100)                         not null,
    aut_key    varchar(100)                         not null,
    validate   tinyint(1) default 0                 null,
    created_at timestamp  default CURRENT_TIMESTAMP null,
    update_at  timestamp  default CURRENT_TIMESTAMP null,
    constraint email_email_uindex
        unique (email)
)
    comment 'E-mail';

INSERT INTO email.email (id, user_name, email, aut_key, validate, created_at, update_at) VALUES (12, 'test', 'larinn60@gmail.com', 'RxrhzcNrV7ywDc1d5un6', 1, '2019-12-09 15:47:39', '2019-12-09 15:48:01');
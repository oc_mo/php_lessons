<?php

abstract class Product
{
    public $name;
    public $price;
    public $weight;
    public $image;

    function __construct(string $name,int $price,int $weight, string $image)
    {
        $this->name=$name;
        $this->price=$price;
        $this->weight=$weight;
        $this->image=$image;
    }

    function output ()
    {
        echo 'Название товара: '.$this->name.' <br> Вес товара: '.$this->weight;
    }

    function taxFree ()
    {
        $tax=$this->price-$this->price*0.2;
        echo '<br> Цена товара c НДС: '.$this->price.' рубелй <br> Цена товара без НДС: '.$tax.' рублей <br><br>';
    }

    function  count()
    {
       echo $this->i;
    }

    abstract public function showImage();


}
class Chocolate extends Product
{
    public $calories;
    public function __construct(string $name, int $price, int $weight, int $calories, string $image)
    {
        $this->calories = $calories;
        parent::__construct($name, $price, $weight, $image);
    }

    public function output ()
    {
        echo 'Название товара: '.$this->name.'<br> Вес товара: '.$this->weight.'<br> Калорийность: '.$this->calories;
    }

    public function showImage()
    {
        echo "<div>
        <img src='{$this->image}' alt='Шоколадка' width='200' height='200'>
        </div>";
    }

}

class Candy extends  Product
{
    public function showImage()
    {
        echo "<div>
        <img src='{$this->image}' alt='Конфеты' width='100' height='100'>
        </div>";
    }
}

$choco1 = new Chocolate('Вкусная шоколадка','140','250','300','choco.jpg');
$choco1->output();
$choco1->taxFree();
$choco1->showImage();
$candy1 = new Candy('Вкусные конфеты','500','1000','candy.jpg');
$candy1 ->output();
$candy1->taxFree();
$candy1 ->showImage();


